﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace Todos {
    // Zadaci.cs
    class Zadaci {

        #region DohvatiZadatke()
        public static List<Zadatak> DohvatiZadatke() {
            List<Zadatak> zadaci = new List<Zadatak>();
            string query = "SELECT * FROM Zadaci";

            using (SqlConnection sqlConnection = new SqlConnection(Baza.KonekcijskiString())) {
                SqlDataAdapter adapter = new SqlDataAdapter(query, sqlConnection);
                DataSet ds = new DataSet();
                adapter.Fill(ds);
                foreach (DataRow redak in ds.Tables[0].Rows) {
                    zadaci.Add(new Zadatak(
                        redak["Id"],
                        redak["Zadatak"],
                        redak["Zavrseno"],
                        redak["Izradeno"]
                    ));
                }
            }
            return zadaci;
        }
        #endregion DohvatiZadatke()

        #region UnesiZadatak()
        public static bool UnesiZadatak(Zadatak zadatak) {
            string query = "INSERT INTO Zadaci (Zadatak, Zavrseno, Izradeno) VALUES (@Zadatak, @Zavrseno, @Izradeno)";

            using (SqlConnection sqlConnection = new SqlConnection(Baza.KonekcijskiString())) {
                using (SqlCommand command = new SqlCommand(query, sqlConnection)) {
                    command.Parameters.AddWithValue("@Zadatak", zadatak.NazivZadatka);
                    command.Parameters.AddWithValue("@Zavrseno", zadatak.Zavrseno);
                    command.Parameters.AddWithValue("@Izradeno", zadatak.Izradeno);

                    sqlConnection.Open();
                    int result = command.ExecuteNonQuery();
                    if (result < 0) {
                        return false;
                    }
                }
            }
            return true;
        }
        #endregion UnesiZadatak()

        #region AzurirajZadatak()
        public static bool AzurirajZadatak(Zadatak zadatak) {
            string query = "UPDATE Zadaci SET Zadatak = @Zadatak, Zavrseno = @Zavrseno, Izradeno = @Izradeno WHERE Id = @Id";

            using (SqlConnection sqlConnection = new SqlConnection(Baza.KonekcijskiString())) {
                using (SqlCommand command = new SqlCommand(query, sqlConnection)) {
                    command.Parameters.AddWithValue("@Id", zadatak.Id);
                    command.Parameters.AddWithValue("@Zadatak", zadatak.NazivZadatka);
                    command.Parameters.AddWithValue("@Zavrseno", zadatak.Zavrseno);
                    command.Parameters.AddWithValue("@Izradeno", zadatak.Izradeno);

                    sqlConnection.Open();
                    int result = command.ExecuteNonQuery();
                    if (result < 0) {
                        return false;
                    }
                }
            }
            return true;
        }
        #endregion AzurirajZadatak()

        public static bool ObrisiZadatak(Zadatak zadatak) {
            string query = "DELETE FROM Zadaci WHERE Id = @Id";

            using (SqlConnection sqlConnection = new SqlConnection(Baza.KonekcijskiString())) {
                using (SqlCommand command = new SqlCommand(query, sqlConnection)) {
                    command.Parameters.AddWithValue("@Id", zadatak.Id);

                    sqlConnection.Open();
                    int result = command.ExecuteNonQuery();
                    if (result < 0) {
                        return false;
                    }
                }
            }
            return true;
        }
    }
}
